import requests


headers = {
    'Accept': 'application/json, text/plain, */*',
    'Referer': 'https://bankrot.fedresurs.ru/tradeplaces',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36',
}


def get_trading_platform(limit: int = 15, offset: int = 45):
    params = {
        'limit': limit,
        'offset': offset,
        'isActive': 'true',
    }
    url = 'https://bankrot.fedresurs.ru/backend/tradeplaces'
    response = requests.get(url = url, params = params, headers = headers)
    return response.json()


data = get_trading_platform(offset = 15)

print(data)
