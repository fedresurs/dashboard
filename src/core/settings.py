import os

from pathlib import Path
from typing import Union, List

from pydantic_settings import BaseSettings
from pydantic import (
    Field,
    AnyHttpUrl,
    MongoDsn,
    root_validator,
    model_validator
)

DEBUG = True

BASE_DIR = Path(os.path.dirname(os.path.dirname(__file__)))


class Base(BaseSettings):

    class Config:
        env_file = os.path.join(BASE_DIR.parent.parent, '.env')


class UvicornSettings(Base):
    app: str = "app.main:app"
    host: str = "0.0.0.0"
    port: int = 8000
    reload: bool = True
    workers: int = 3

    class Config:
        env_prefix = "UVICORN_"


class MongoSettings(Base):
    db_name: str

    protocol: str = "mongodb"
    username: str = Field(validation_alias = "MONGO_INITDB_ROOT_USERNAME")
    password: str = Field(validation_alias = "MONGO_INITDB_ROOT_PASSWORD")
    host: str = '127.0.0.1'
    port: int = 27017
    path: str = 'admin'

    dsn: str = ''

    @model_validator(mode='after')
    def check_passwords_match(self) -> 'MongoSettings':
        self.dsn = "{protocol}://{username}:{password}@{host}:{port}/{path}".format(
            username = self.username,
            password = self.password,
            protocol = self.protocol,
            host = self.host,
            port = self.port,
            path = self.path,
        )
        return self

    class Config:
        env_prefix = "MONGO_"


class SecuritySettings(Base):
    admin: str = 'admin'
    secret: str = "$2b$12$XCGB7PmHcV6PCXFQ7yxWJO"
    ALLOWED_HOSTS: Union[str, List[AnyHttpUrl]] = []

    class Config:
        env_prefix = "SECURITY_"


class Settings(Base):
    project_name: str = 'falcon-project.ru'

    mongo: MongoSettings = MongoSettings()

    base_dir: Path = BASE_DIR
    api_v1_str: str = "v1"
